#!/usr/bin/env python
'''
Formatter for latex output.
'''

try:
  from .. import helper
except ImportError as e:
  print(f"Error importing: {__file__}. {e}")
try:
  from .baseformatter import BaseFormatter
except ImportError as e:
  print(f"Error importing: {__file__}. {e}")


# def fmt_literal(self, f, value):
#   if value:
#     return f"{self.indent*' '}{f} = {{{value}}},") + "\n"
#   else:
#     return ""

def fmt_title(f, e):
  """ title in latex form

  Parameters
  ----------
  e : entry

  f : field name

  Returns
  -------

  str:
    Formatted title
  """
  thetitle = helper.handle_math(e.get('title', ''), 1)
  if thetitle:
    return helper.add_math(rf"\textbf{{{thetitle}}} ")
  else:
    return ""


def fmt_doi(f, e):
  doi = e.get(f, "")
  if doi:
    url = f"http://dx.doi.org/{doi}"
  else:
    url = e.get("url", "")

  if url:
    return f' \\url{{{url}}}'
  else:
    return ""


t2l = helper.convert2latex


class LatexFormatter(BaseFormatter):
  """Class the encapsulates all that is needed to format a single entry in LaTeX form
  """

  def __init__(self):
    super(LatexFormatter, self).__init__()
    self.fields = [
      # '_code',
      'title',
      'author',
      'journal',
      'booktitle',
      'school',
      'volume',
      'number',
      'pages',
      'year',
      'doi',
    ]

    # Two arguments: (field, entry). Format quite verbatim "k = f"
    self.field_formatter = {k: lambda f, e: e.get(f, "") for k in self.fields}
    formatter = {
      "author": lambda f, e: rf"\authors{{{t2l(e.get_authors())}}}. ",
      "editor": lambda f, e: rf"\authors{{{t2l(e.get_authors(who='editor'))}}}",
      "title": fmt_title,
      "year": lambda f, e: e.getyear(),
      "volume": lambda f, e: rf"\textbf{{{e.get(f,'')}}}" if e.get(f, '') else "",
      "number": lambda f, e: rf"({e.get(f,'')}) " if e.get(f, '') else " ",
      "journal": lambda f, e: rf" \emph{{{e.get(f,'')}}} " if e.get(f, '') else "",
      "pages": lambda f, e: e.getpages()+", " if e.getpages() != '' else '',
      "doi": fmt_doi,
    }
    self.field_formatter.update(formatter)
