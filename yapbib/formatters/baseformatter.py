#!/usr/bin/env python
'''
Formatters for different outputs.

Intended supported formats are:

  - bibtex
  - latex
  - html
  - xml
  - full (ad-hoc)
  - short (ad-hoc)
'''
import textwrap

try:
  from .. import helper
except ImportError as e:
  print(f"Error importing: {__file__}. {e}")


class BaseFormatter(object):
  """Class the encapsulates all that is needed to format a single entry
  """

  def __init__(self):
    super(BaseFormatter, self).__init__()

    # List of all biblio-py fields.
    # Most likely will be overwritten by each particular formatter
    self.fields = helper.allfields.copy()
    # # We initialize with verbatim values of the field
    # self.field_formatter = {k: str for k in self.fields}
    # Takes two arguments: (field, entry) and returns empty string "" if k is not present
    self.field_formatter = {k: lambda k, e: f"{k}: {str(e.get(k,''))}\n" for k in self.fields}
    # Formatting parameters
    self.set_formatting()

  def set_formatting(self, width=80, indent=2, encoding='utf-8'):
    """Set formatting parameters

    Parameters
    ----------
    width : Total width of values text

    indent : number of spaces for indentation
    """
    self.width = width
    self.indent = indent
    self.encoding = encoding
    initial_indent = self.indent*" "
    self.wrap = textwrap.TextWrapper(width=self.width, subsequent_indent=3 * initial_indent,
                                     break_long_words=False)

  def to_string(self, entry, beginning="", ending="\n"):
    """Convert to a string a formatted entry

    Parameters
    ----------
    entry : bibitem entry

    beginning : str

    ending : str

    Returns
    -------

    string with the formatted entry
    """
    s = beginning
    for kk in self.fields:
      if entry:
        fs = self.field_formatter[kk](kk, entry)
        if fs:
          s += fs

    # Close the entry
    s += ending
    return s
