#!/usr/bin/env python
'''
Formatter for bibtex output.
'''

try:
  from .. import helper
except ImportError as e:
  print(f"Error importing: {__file__}. {e}")
try:
  from .baseformatter import BaseFormatter
except ImportError as e:
  print(f"Error importing: {__file__}. {e}")


def fmt_authors(list_aut, who):
  """ authors in xml form

  Parameters
  ----------
  auth_list : authors list


  Returns
  -------

  str:
    Formatted list of authors
  """
  form_aut = f'<{who}s>\n'
  for a in list_aut:
    form_aut += f'    <{who}>{a}</{who}>\n'

  return form_aut + f"  </{who}s>\n"


def fmt_literal(f, value):
  if value:
    return f'  <{f}>{value}</{f}>\n'
  else:
    return ""


def fmt_code(f, e):
  "_code and _type"
  t = "_type"
  return f'  <entry id="{e[f]}">\n  <{e[t]}>\n' + fmt_literal(f, e[f])


class XmlFormatter(BaseFormatter):
  """Class the encapsulates all that is needed to format a single entry in bibtex form
  """

  def __init__(self):
    super(XmlFormatter, self).__init__()
    self.fields = ['_code', '_type', 'author', 'title', 'journal', 'volume', 'number', 'booktitle',
                   'chapter', 'address', 'edition', 'howpublished', 'school', 'institution',
                   'organization', 'publisher', 'series', 'pages',
                   'date', 'issn', 'isbn', 'doi', 'abstract', 'closing']

    # Two arguments: (field, entry). Format quite verbatim "k = f"
    self.field_formatter = {k: lambda f, e: fmt_literal(f, e.get(f, "")) for k in self.fields}
    formatter = {
      "_code": fmt_code,
      "author": lambda f, e: f"{self.indent*' '}{fmt_authors(e.get_authorsList(Initial=True, who=f), f)}",
      "editor": lambda f, e: f"{self.indent*' '}{fmt_authors(e.get_authorsList(Initial=True, who=f), f)}",
      "date": lambda f, e: fmt_literal("date", e.getdate()),
      "pages": lambda f, e: f"{fmt_literal(f, e.getpages())}",
      "closing": lambda f, e: f"  </{e['_type']}>\n  </entry>\n",
    }
    self.field_formatter.update(formatter)
