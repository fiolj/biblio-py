#!/usr/bin/env python
'''
Formatter for bibtex output.
'''

try:
  from .. import helper
except ImportError as e:
  print(f"Error importing: {__file__}. {e}")
try:
  from .baseformatter import BaseFormatter
except ImportError as e:
  print(f"Error importing: {__file__}. {e}")

MAX_AUTHORS = 10


def fmt_authors(list_aut):
  """ authors in html form

  Parameters
  ----------
  auth_list : authors list


  Returns
  -------

  str:
    Formatted list of authors
  """
  if len(list_aut) > 10:
    list_aut = list_aut[:MAX_AUTHORS]
    list_aut.append('<i>et al</i>')

  form_aut = '<span class="authors">'
  for a in list_aut:
    form_aut += '<span class="author">' + a + '</span>, '

  form_aut = form_aut[:-2] + "</span> "
  return helper.replace_tags(form_aut, "latex")


def fmt_abstract(f, e):
  """ authors in html form

  Parameters
  ----------
  e : entry

  Returns
  -------

  str:
    Formatted abstract
  """
  thetext = helper.removebraces(helper.handle_math(e.get(f, "")))  # abstract text
  if thetext:
    theid = e.get("_code")                                # key
    start = f'<div class="abstracts button" onClick="toggle(\'{theid}\')">{f.upper()}</div>\n'
    return f'{start}<div class="{f}" id="{theid}">{thetext}</div>'
  else:
    return ""


def fmt_doi(f, e):
  doi = e.get(f, "")
  if doi:
    return f'  <a class="button" title="Get from source" href="http://dx.doi.org/{doi} ">DOI</a> '
  else:
    url = e.get("url", "")
    if url:
      return f'  <a class="button" title="Get from source" href="{url} ">URL</a> '
    return ""


# def fmt_code(f, e):
#   return f' <class="{f}" title="code">´{e[f]}</class>'
#   fn = f'./{e[f]}.pdf'
#   if "file" in e:
#     fn = e.get("file", fn)
#   return f'  <a class="file" title="Download" href="{fn}">{e[f]}</a> '


def fmt_file(f, e):
  if "file" in e:
    fn = e.get("file", e)
    return f'  <a class="file" title="Open document" href="{fn}"a>Open</a> '
  else:
    return ""


class HtmlFormatter(BaseFormatter):
  """Class the encapsulates all that is needed to format a single entry in bibtex form
  """

  def __init__(self):
    super(HtmlFormatter, self).__init__()
    self.fields = ['_type', 'title', 'author', 'journal', 'volume', 'number', 'booktitle',
                   'chapter', 'address', 'edition', 'howpublished', 'school', 'institution',
                   'organization', 'publisher', 'series', 'pages',
                   'month', 'year', 'issn', 'isbn', 'doi', '_code', 'abstract', 'file']

    # Two arguments: (field, entry). Format quite verbatim "k = f"
    self.field_formatter = {k: lambda f, e: self.fmt_literal(f, e.get(f, "")) for k in self.fields}
    formatter = {
      # "_code": fmt_code,
      "_type": lambda f, e: f'<li class="{e[f]}"> ',
      "author": lambda f, e: fmt_authors(e.get_authorsList(Initial=True, who=f)),
      "title": lambda f, e: self.fmt_literal(f, helper.removebraces(helper.handle_math(e.get(f, "")))),
      "editor": lambda f, e: fmt_authors(e.get_authorsList(Initial=True, who=f)),
      "abstract": fmt_abstract,
      "year": lambda f, e: self.fmt_literal(f, e.getyear()),
      "month": lambda f, e: self.fmt_literal(f, e.getmonth()),
      # Note that biblatex accepts date instead of day,month,year
      # "date": lambda f, e: self.fmt_literal("date", e.getdate()),
      # "pages": lambda f, e: (" pag. " + self.fmt_literal(f, e.getpages()) + ". ").strip("pag. ."),
      "pages": lambda f, e: " pag. " + self.fmt_literal(f, e.getpages()) + ". " if e.getpages() != '' else '',
      "keywords": lambda f, e: self.fmt_literal(f, ', '.join(e.get(f, []))),
      "affiliations": lambda f, e: self.fmt_literal(f, ', '.join(e.get(f, []))),
      "file": fmt_file,
      "doi": fmt_doi,
      "issn": lambda f, e: "",
      "isbn": lambda f, e: "",
    }
    self.field_formatter.update(formatter)

  def fmt_literal(self, f, value):
    if value:
      return f'<span class="{f}">{value}</span> '
    else:
      return ""
