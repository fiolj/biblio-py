#!/usr/bin/env python
'''
Formatter for bibtex output.
'''
import re
try:
  from .. import helper
except ImportError as e:
  print(f"Error importing: {__file__}. {e}")
try:
  from .baseformatter import BaseFormatter
except ImportError as e:
  print(f"Error importing: {__file__}. {e}")


def fmt_title(f, e):
  tit = helper.cap_rex.sub(r'\1', e.get(f, "")).strip()
  if tit:
    # Add braces to single upper-case letters
    return tit[0] + re.sub(r"([A-ZÑ])", r"{\1}", tit[1:])


class BibtexFormatter(BaseFormatter):
  """Class the encapsulates all that is needed to format a single entry in bibtex form
  """

  def __init__(self):
    super(BibtexFormatter, self).__init__()
    self.fields = helper.bibtexfields.copy()
    # Two arguments: (field, entry). Formatted almost verbatim "k = f"
    self.field_formatter = {k: lambda f, e: self.fmt_literal(f, e.get(f, "")) for k in self.fields}
    formatter = {
      "_code": lambda f, e: f"{e[f]},\n",
      "_type": lambda f, e: f"@{e[f]}{{".upper(),
      "author": lambda f, e: self.fmt_literal(f, ' and '.join(e.get_authorsList(format=1, who=f))),
      "editor": lambda f, e: self.fmt_literal(f, ' and '.join(e.get_authorsList(format=1, who=f))),
      "year": lambda f, e: self.fmt_literal(f, e.getyear()),
      "month": lambda f, e: self.fmt_literal(f, e.getmonth()),
      "day": lambda f, e: self.fmt_literal(f, e.getday()),
      # Note that biblatex accepts date instead of day,month,year
      # "date": lambda f, e: self.fmt_literal("date", e.getdate()),
      "pages": lambda f, e: self.fmt_literal(f, e.getpages()),
      "affiliation": lambda f, e: self.fmt_literal(f, ', '.join(e.get(f, []))),
      "keywords": lambda f, e: self.fmt_literal(f, ', '.join(e.get(f, []))),
      "title": lambda f, e: self.fmt_literal(f, fmt_title(f, e)),
      "abstract": lambda f, e: self.fmt_literal(f, fmt_title(f, e)),
    }
    self.field_formatter.update(formatter)

  def fmt_literal(self, f, value):
    if value:
      return self.wrap.fill(f"{self.indent*' '}{f} = {{{helper.convert2bibtex(value)}}},") + "\n"
    else:
      return ""
