#! /usr/bin/env python

import re
# from . import helper
from .. import helper
from .baseparser import BiblioParser


class PubmedParser(BiblioParser):
  """Pubmed Bibliography parser
  """

  def __init__(self):
    super(PubmedParser, self).__init__()

    # Regular expressions for parsing of the entries
    # Signals init of entry
    self.reg_begin = re.compile("^\\s+(PMID- )", re.MULTILINE | re.UNICODE)
    # Signals a field
    self.reg_field = re.compile("^([A-Z ]{4}- )", re.MULTILINE | re.UNICODE)

    self.field_translator.update({
        "_code": self.get_code,
        "_type": self.get_worktype,
        "type": self.get_types,
        'abstract': lambda: self.get_literal('AB'),
        'affiliation': self.get_authorsaffil,
        'author': self.get_authors,
        'booktitle': lambda: self.get_literal('BTI'),
        'date': self.get_date,
        'doi': self.get_doi,
        'edition': lambda: self.get_literal('EN'),
        'editor': lambda: self.get_literal('FED'),
        # 'isbn': lambda: self.get_literal('ISBN'),
        # 'issn': lambda: self.get_literal('IS'),
        'isbn': lambda: ", ".join(self.get_literal('ISBN')),
        'issn': lambda: ", ".join(self.get_literal('IS')),
        'journal': lambda: self.get_literal('JT'),
        'journalabbrev': lambda: self.get_literal('TA'),
        'keywords': lambda: self.get_literal('OT'),
        'number': lambda: self.get_literal('IP'),
        'organization': lambda: self.get_literal('IRAD'),
        'pages': self.get_pages,   # From 'PG'
        'publisher': self.get_publisher,   # From 'AB'
        'pubstate': lambda: self.get_literal('PST'),
        'series': lambda: self.get_literal('CTI'),
        'title': lambda: self.get_literal('TI'),
        'volume': lambda: self.get_literal('VI'),
    })

  def get_date(self):
    """Get the publication day

    Returns
    -------

    string: the day in format 'dd'
    """
    d = self.get_literal('DP')
    mm = dd = yy = ""
    if d:
      t = d.split()
      if len(t) >= 3:
        dd = t[2].zfill(2)
      if len(t) >= 2:
        mm = f"{[x[0] for x in helper.strings_month].index(t[1].lower()) + 1:02d}"
      if len(t) >= 1:
        yy = t[0].zfill(4)
      return [yy, mm, dd]
    else:                         # Try to get if from Electronic Publication Date
      d = self.get_literal('DEP')
      if d:
        return [d[:4], d[4:6], d[6:8]]

  def get_code(self):
    """Get the work type (article, book, etc)

    Returns
    -------

    string: the type of work. Will be one of the known types
    """
    code = self.get_literal("PMID")
    return code

  def get_worktype(self):
    """Get the work type (article, book, etc)

    Returns
    -------

    string: the type of work. Will be one of the known types
    """
    # First we flatten the list (each word an item)

    a = self.get_literal("PT")
    try:                        # is a list
      a = [] + a
    except BaseException:
      a = [a]                   # is a string
    t = " ".join(a).lower().split()

    for v in t:
      if v in helper.work_types:
        return v

    if self.get_literal("JT"):
      return "article"
    elif self.get_literal("CTI"):
      return "incollection"
    elif self.get_literal("BTI"):
      return "book"
    return "misc"

  def get_types(self):
    """Get the work types as a list. It can be more than one (compatible) type

    Returns
    -------

    list: the types of work

    Examples
    --------

      ["Journal Article", "Review"]

    or

      ["Book", "Ebook"]

    """
    return self.get_literal("PT")

  def get_authors(self):
    """Get the author list

    Each author is a list of the form:

                      [von, Last, First, Jr]

    Returns
    -------

    list: where each element is a list
    """
    d = self.get_firstliteral(["FAU", "AU"])
    return list(map(helper.process_name, d))

  def get_publisher(self):
    """Get the publisher of the book or similar

    Taken from pubmed Copyright informaction (CI)

    Returns
    -------

    list or string?
    """
    return self.get_firstliteral(["CI", "OCI"])

  def get_authorsaffil(self):
    """Get the author affiliation list

    Uses the pubmed fields "IRAD" (researcher affiliation) or "AD" (author affiliation address)

    Returns
    -------

    list:  affiliations
    """

    return self.get_firstliteral(["IRAD", "AD"])

  def get_doi(self):
    """Get the DOI of the work

    Returns
    -------

    string: DOI resource
    """
    d = self.get_literal("AID")
    if d:
      for v in d:
        if "doi" in v:
          return v.split()[0]

  def get_pages(self):
    """Get the pages of the work

    Returns
    -------

    list: ['first page', 'last page', 'total pages'] each element type is string

    """
    fpage = lpage = tpage = ""

    d = self.get_literal("PG")
    pgs = re.split(r"\W+", d)
    # pgs = d.split("-")

    if pgs:
      fpage = pgs[0]

    if len(pgs) > 1:
      try:
        fp = int(pgs[0])
        lp = int(pgs[1])
        while lp < fp:  # some digit was omitted
          lp += 10
        lpage = str(lp)
        tpage = str(lp - fp)
      except BaseException:
        lpage = ""

    return [fpage, lpage, tpage]

  # def separate_entries(self, page):
  #   """Reads a string and identifies bibliographic entries

  #   Parameters
  #   ----------
  #   entry : string

  #   Returns
  #   -------

  #   item: list
  #      list of strings with the text of each entry
  #   """
  #   return ["PMID- " + p for p in page.split("PMID- ")[1:]]


if __name__ == '__main__':

  # from pathlib import Path
  from io import StringIO

  pubmedinput = """

PMID- 33974091
OWN - NLM
STAT- MEDLINE
DCOM- 20210712
LR  - 20220513
IS  - 1619-7089 (Electronic)
IS  - 1619-7070 (Print)
IS  - 1619-7070 (Linking)
VI  - 48
IP  - 8
DP  - 2021 Jul
TI  - Challenges and future options for the production of lutetium-177.
PG  - 2329-2335
LID - 10.1007/s00259-021-05392-2 [doi]
FAU - Vogel, W V
AU  - Vogel WV
AD  - Department of Nuclear Medicine, The Netherlands Cancer Institute-Antoni van
      Leeuwenhoek (NKI-AVL), Plesmanlaan 121, 1066, CX, Amsterdam, the Netherlands.
      w.vogel@nki.nl.
FAU - van der Marck, S C
AU  - van der Marck SC
AD  - Nuclear Research and consultancy Group (NRG), Petten, Netherlands.
FAU - Versleijen, M W J
AU  - Versleijen MWJ
AD  - Department of Nuclear Medicine, The Netherlands Cancer Institute-Antoni van
      Leeuwenhoek (NKI-AVL), Plesmanlaan 121, 1066, CX, Amsterdam, the Netherlands.
LA  - eng
PT  - Editorial
PL  - Germany
TA  - Eur J Nucl Med Mol Imaging
JT  - European journal of nuclear medicine and molecular imaging
JID - 101140988
RN  - 0 (Radioisotopes)
RN  - 5H0DOZ21UJ (Lutetium)
RN  - BRH40Y9V1Q (Lutetium-177)
SB  - IM
MH  - Humans
MH  - *Lutetium
MH  - *Radioisotopes
PMC - PMC8241800
COIS- S.C. van der Marck is employed at one of the production facilities for
      lutetium-177 (NRG). W.V. Vogel and M.W.J. Versleijen are involved in clinical
      care and scientific research with lutetium-177 (NKI-AVL).
EDAT- 2021/05/12 06:00
MHDA- 2021/07/13 06:00
CRDT- 2021/05/11 12:20
PHST- 2021/05/12 06:00 [pubmed]
PHST- 2021/07/13 06:00 [medline]
PHST- 2021/05/11 12:20 [entrez]
AID - 10.1007/s00259-021-05392-2 [pii]
AID - 5392 [pii]
AID - 10.1007/s00259-021-05392-2 [doi]
PST - ppublish
SO  - Eur J Nucl Med Mol Imaging. 2021 Jul;48(8):2329-2335. doi:
      10.1007/s00259-021-05392-2.

PMID- 27929488
OWN - NLM
STAT- MEDLINE
DCOM- 20170425
LR  - 20220331
IS  - 1526-9914 (Electronic)
IS  - 1526-9914 (Linking)
VI  - 17
IP  - 6
DP  - 2016 Nov 8
TI  - Production and quality control 177Lu (NCA)-DOTMP as a potential agent for bone
      pain palliation.
PG  - 128-139
LID - 10.1120/jacmp.v17i6.6375 [doi]
AB  - Skeletal uptake of radiolabeled-1, 4, 7, 10-tetraazacyclododecane-1, 4, 7,
      10-tetramethylene phosphoric acid (e.g., 177Lu-DOTMP) complex, is used for bone
      pain palliation. The moderate energy of β-emitting 177Lu (T½ = 6.7 d, Eβmax =
      497keV) has been considered as a potential radionuclide for development of the
      bone-seeking radiopharmaceutical. Since the specific activity of the radiolabeled
      carrier molecules should be high, the "no-carrier-added radionuclides" have
      sig-nificant roles in nuclear medicine. Many researchers illustrated
      no-carrier-added 177Lu production; among these separation techniques such as ion
      exchange chromatography, reversed phase ion-pair, and electrochemical method,
      extraction chromatography has been considered more capable than other methods. In
      order to optimize the conditions, some effective factors on separation of Lu/Yb
      were investigated by EXC. The NCA 177Lu, produced by this method, was mixed with
      300 μl of DOTMP solution (20 mg in 1 mL of 0.5 M NaHCO3, pH = 8) and incu-bated
      under stirring at room temperature for 45 min. Radiochemical purity of the
      177Lu-DOTMP complex was determined using radio-thin-layer chromatography (RTLC)
      method. The complex was injected to wild-type rats and biodistribution was then
      studied for seven days. The NCA 177Lu was produced with specific activ-ity of 48
      Ci/mg and with a radinuclidic purity of 99.99% through irradiation of enriched
      176Yb target (1 mg) in a thermal neutron flux of 4 × 1013 n.cm-2.s-1 for 14 days.
      177Lu-DOTMP was obtained with high radiochemical purities (> 98%) under optimized
      reaction conditions. The radiolabeled complex exhibited excellent stability at
      room temperature. Biodistribution of the radiolabeled complex studies in rats
      showed favorable selective skeletal uptake with rapid clearance from blood along
      with insignificant accumulation within the other nontargeted organs.
CI  - © 2016 The Authors.
FAU - Salek, Nafise
AU  - Salek N
AD  - Amirkabir University of Technology. n.salek88@yahoo.com.
FAU - Shamsaei, Mojtaba
AU  - Shamsaei M
FAU - Ghannadi Maragheh, Mohammad
AU  - Ghannadi Maragheh M
FAU - Shirvani Arani, Simindokht
AU  - Shirvani Arani S
FAU - Bahrami Samani, Ali
AU  - Bahrami Samani A
LA  - eng
PT  - Journal Article
DEP - 20161108
PL  - United States
TA  - J Appl Clin Med Phys
JT  - Journal of applied clinical medical physics
JID - 101089176
RN  - 0 (177Lu-1,4,7,10-tetraazacyclododecane-1,4,7,10-tetrakis(methylenephosphonic
      acid))
RN  - 0 (Organometallic Compounds)
RN  - 0 (Radiopharmaceuticals)
SB  - IM
MH  - Animals
MH  - Bone and Bones/diagnostic imaging/*metabolism/radiation effects
MH  - Organometallic Compounds/*therapeutic use
MH  - Pain/diagnostic imaging/metabolism/*radiotherapy
MH  - Pain Management
MH  - Palliative Care/*methods
MH  - *Quality Control
MH  - Radionuclide Imaging
MH  - Radiopharmaceuticals/*therapeutic use
MH  - Rats
MH  - Rats, Wistar
PMC - PMC5690526
EDAT- 2016/12/09 06:00
MHDA- 2017/04/26 06:00
CRDT- 2016/12/09 06:00
PHST- 2016/03/14 00:00 [received]
PHST- 2016/07/18 00:00 [accepted]
PHST- 2016/07/23 00:00 [revised]
PHST- 2016/12/09 06:00 [entrez]
PHST- 2016/12/09 06:00 [pubmed]
PHST- 2017/04/26 06:00 [medline]
AID - ACM20128 [pii]
AID - 10.1120/jacmp.v17i6.6375 [doi]
PST - epublish
SO  - J Appl Clin Med Phys. 2016 Nov 8;17(6):128-139. doi: 10.1120/jacmp.v17i6.6375.


  """

  # fname = Path("ejemplos/ejemplo1.nbib")
  fname = StringIO(pubmedinput)
  BPM = PubmedParser()
  entries = BPM.separate_entries(pubmedinput)
  bib = BPM.parse_file(fname)
