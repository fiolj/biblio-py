#! /usr/bin/env python

import re

# from . import helper
# from . import baseparser
from .. import helper
from .baseparser import BiblioParser


class ADSParser(BiblioParser):
  """Pubmed Bibliography parser
  """

  def __init__(self):
    super(ADSParser, self).__init__()
    # Mapping from pubmed to internal

    # Regular expressions for parsing of the entries
    # Signals init of entry
    self.reg_begin = re.compile('^[\\s]*(%R )', re.MULTILINE | re.UNICODE)
    # Signals a field
    self.reg_field = re.compile('^[\\s]*%([RAaFJVDPLTtCOQGSEIUKMNXWYBZ]) ',
                                re.MULTILINE | re.UNICODE)

    self.field_translator.update({
        "_code": self.get_code,
        "_type": self.get_worktype,
        "type": self.get_worktype,
        "abstract": lambda: self.get_literal("B"),
        "affiliation": self.get_authorsaffil,
        "author": self.get_authors,
        # "booktitle": lambda: self.get_literal("T"),
        "date": self.get_date,
        "doi": self.get_doi,
        # "edition": lambda: self.get_literal("EN"),
        # "editor": lambda: self.get_literal("FED"),
        # "isbn": lambda: self.get_literal("ISBN"),
        # "issn": lambda: self.get_literal("IS"),
        "journal": self.get_journal,
        # "journalabbrev": lambda: self.get_literal("TA"),
        "keywords": self.get_keywords,
        "number": self.get_number,
        # "organization": lambda: self.get_literal("IRAD"),
        "pages": self.get_pages,   # From "PG"
        # "publisher": self.get_publisher,   # From "AB"
        # "pubstate": lambda: self.get_literal("PST"),
        # "series": lambda: self.get_literal("CTI"),
        "title": lambda: self.get_literal("T"),
        "volume": lambda: self.get_literal("V"),
    })

  def get_keywords(self):
    """Get the keywords

    Returns
    -------

    list: a list of keywords
    """
    k = self.get_literal("K")
    if k:
      return list(map(str.strip, k.split(",")))
    else:
      return []

  def get_date(self):
    """Get the publication day

    Returns
    -------

    string: the day in format 'dd'
    """
    d = self.get_literal('D')
    mm = dd = yy = ""
    if d:
      t = d.split("/")
      t.reverse()
      if len(t) >= 3:
        dd = t[2].zfill(2)
      if len(t) >= 2:
        mm = t[1].zfill(2)
      if len(t) >= 1:
        yy = t[0].zfill(4)
      return [yy, mm, dd]
    else:                         # Try to get if from Electronic Publication Date
      d = self.get_literal('DEP')
      if d:
        return [d[:4], d[4:6], d[6:8]]

  def get_code(self):
    """Get the work type (article, book, etc)

    Returns
    -------

    string: the type of work. Will be one of the known types
    """
    code = self.get_literal("R")
    return code

  def get_journal(self):
    """Get Journal from ads format

    Parameters
    ----------
    t : string
    """
    t = self.get_literal("J")
    indexes = [t.find(v) for v in ["Vol", "Issue", "pp"] if t.find(v) > 0]
    if indexes:
      j = min(indexes)
      t = t[0:j]
      return t.strip().strip(',')

  def get_number(self):
    """Get Journal number from ads format

    Parameters
    ----------
    t : string
    """
    t = self.get_literal("J")
    issue = t.split("Issue")[-1].split()[0]
    return issue.strip(",")

  def get_worktype(self):
    """Get the work type (article, book, etc)

    Returns
    -------

    string: the type of work. Will be one of the known types
    """
    # First we flatten the list (each word an item)

    if self.get_literal("a"):
      return "book"
    journal = self.get_journal()
    if journal:
      if "thesis" in journal.lower():
        return "phdthesis"
      else:
        return "article"
    return "misc"

  # def get_types(self):
  #   """Get the work types as a list. It can be more than one (compatible) type

  #   Returns
  #   -------

  #   list: the types of work

  #   Examples
  #   --------

  #     ["Journal Article", "Review"]

  #   or

  #     ["Book", "Ebook"]

  #   """
  #   return self.get_literal("PT")

  def get_authors(self):
    """Get the author list

    Each author is a list of the form:

                      [von, Last, First, Jr]

    Returns
    -------

    list: where each element is a list
    """
    d = self.get_firstliteral(["A", "a"]).split(";")
    # return d
    return list(map(helper.process_name, d))

  # def get_publisher(self):
  #   """Get the publisher of the book or similar

  #   Taken from pubmed Copyright informaction (CI)

  #   Returns
  #   -------

  #   list or string?
  #   """
  #   return self.get_firstliteral(["CI", "OCI"])

  def get_authorsaffil(self):
    """Get the author affiliation list

  Uses the field "F" for affiliation address

    Returns
    -------

    list:  affiliations
    """
    reg_af = re.compile(
        '[ ]?[A-D][A-Z](?=\\()')  # Regexp: Hasta un espacio, seguido de dos mayusculas, seguido de un parentesis (que no se consume)

    t = self.get_literal("F")
    if t:
      F = reg_af.split(t)
      if len(F) > 1:
        F = [f.strip(",").strip() for f in F[1:]]
      return F
    else:
      return []

  def get_doi(self):
    """Get the DOI of the work

    Returns
    -------

    string: DOI resource
    """
    d = self.get_literal("Y")
    if d:
      return d.split()[-1]

  def get_pages(self):
    """Get the pages of the work

    Returns
    -------

  list: ['first page', 'last page', 'total pages'] each element type is
  string

    """
    lpage = tpage = ""

    fpage = self.get_literal("P")
    lpage = self.get_literal("L")

    if fpage and fpage.isnumeric():
      fp = int(fpage)
    else:
      fp = None

    if lpage and lpage.isnumeric():
      lp = int(lpage)
      if fp:
        tpage = str(lp - fp)

    else:
      lpage = ""

    return [fpage, lpage, tpage]


if __name__ == '__main__':

  # from pathlib import Path
  from io import StringIO

  adsinput = """
%R 2008AmJPh..76.1146M
%T A velocity-dependent potential of a rigid body in a rotating frame
%A Moreno, G. A.; Barrachina, R. O.
%F AA(Departamento de F\xedsica J. J. Giambiagi, Facultad de Ciencias Exactas y
Naturales, UBA, Ciudad Universitaria, Pabell\xf3n I, 1428 Buenos Aires,
Argentina), AB(Centro At\xf3mico Bariloche and Instituto Balseiro, Comisi\xf3n
Nacional de Energ\xeda At\xf3mica and Universidad Nacional de Cuyo, 8400 S. C. de
Bariloche, R\xedo Negro, Argentina)
%J American Journal of Physics, Volume 76, Issue 12, pp. 1146-1149 (2008).
%V 76
%D 12/2008
%P 1146
%L 1149
%K classical mechanics,  physics education,  rotating bodies
%G AIP
%C (c) 2008: American Institute of Physics
%I ABSTRACT: Abstract;
   EJOURNAL: Electronic On-line Article (HTML);
   REFERENCES: References in the Article;
   AR: Also-Read Articles;
%U http://adsabs.harvard.edu/abs/2008AmJPh..76.1146M
%B We derive a velocity-dependent potential for describing the dynamics of
a rigid body in a rotating frame. We show that, as for one-particle
systems, the different components of this potential can be associated
with electromagnetic analogs. We provide some examples to demonstrate
the feasibility of using the potential as an alternative description of
rigid body problems.
%Y DOI: 10.1119/1.2982632

%R 2008JPhB...41n5204M
%T Transfer ionization and total electron emission for 100 keV amu<SUP>-1</SUP>
He<SUP>2+</SUP> colliding on He and H<SUB>2</SUB>
%A Mart\xednez, S.; Bernardi, G.; Focke, P.; Su\xe1rez, S.; Fregenal, D.
%F Centro At\xf3mico Bariloche and Instituto BalseiroComisi\xf3n Nacional de Energ\xeda
At\xf3mica and Universidad Nacional de Cuyo, Argentina., 8400 S C de Bariloche,
R\xedo Negro, Argentina <EMAIL>bernardi@cab.cnea.gov.ar</EMAIL>
%J Journal of Physics B: Atomic, Molecular, and Optical Physics, Volume 41,
Issue 14, pp. 145204 (2008).
%V 41
%D 7/2008
%P 5204
%G IOP
%I ABSTRACT: Abstract;
   EJOURNAL: Electronic On-line Article (HTML);
   REFERENCES: References in the Article;
   AR: Also-Read Articles;
%U http://adsabs.harvard.edu/abs/2008JPhB...41n5204M
%B We have measured electron emission for transfer ionization (TI) and
total electron emission (TEE, all emission processes) for 100 keV
amu<SUP>-1</SUP> He<SUP>2+</SUP> on He and H<SUB>2</SUB> targets. Double
differential cross sections have been obtained for emission angles
\\u03b8 = 0\xb0, 20\xb0 and 45\xb0, and electron energies ranging
from 2 to 300 eV. Pure ionization, mainly due to single ionization,
dominates the low-energy electron emission. The main observed structure
in the electron spectra, a cusp centred at \\u03b8 = 0\xb0 and at a
speed equal to that of the incident projectile, presents an asymmetric
shape. This is in contrast to the symmetric shape observed by us at 25
keV amu<SUP>-1</SUP> for the same collision systems, suggesting a change
in the cusp formation mechanism for TI within this energy range.
%Y DOI: 10.1088/0953-4075/41/14/145204

"""

  # fname = Path("ejemplos/ejemplo1.nbib")
  fname = StringIO(adsinput)
  BPM = ADSParser()
  bib = BPM.parse_file(fname)
