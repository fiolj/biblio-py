#! /usr/bin/env python
# TODO: Fails when string does not have "
import re
from .. import helper
from .baseparser import BiblioParser

# Evaluating change to 'utf-8'
from pylatexenc.latex2text import LatexNodes2Text


class BibtexParser(BiblioParser):
  """BibTeX Bibliography parser
  """

  def __init__(self):
    super(BibtexParser, self).__init__()
    # Mapping from pubmed to internal

    # Regular expression for identifying an entry
    # A '@' followed by any word and an opening {
    self.reg_begin = re.compile(r'\s?@(\w*\s*[{\(])')

    # Signals a field:  word = ("words" or {words}) (with possible spaces)
    self.reg_field = re.compile('^[\\s]*(\\w+)\\s?= ["{}]',
                                re.MULTILINE | re.UNICODE)

    # Conversion rules for each field
    self.field_translator.update({
      "_code": lambda: self.get_literal("_code"),
      "_type": lambda: self.get_literal("_type"),
      "abstract": lambda: self.get_literal("abstract"),
      "address": lambda: self.get_literal("address"),
      "affiliation": lambda: self.get_keyaff("affiliation"),
      "annote": lambda: self.get_literal(""),
      "booktitle": lambda: self.get_literal("booktitle"),
      "chapter": lambda: self.get_literal("chapter"),
      "crossref": lambda: self.get_literal("crossref"),
      "doi": lambda: self.get_literal("doi"),
      "edition": lambda: self.get_literal("edition"),
      "file": lambda: self.get_literal("file"),
      "howpublished": lambda: self.get_literal("howpublished"),
      "institution": lambda: self.get_literal("institution"),
      "isbn": lambda: self.get_literal("isbn"),
      "issn": lambda: self.get_literal("issn"),
      "journal": lambda: self.get_literal("journal"),
      "note": lambda: self.get_literal("note"),
      "number": lambda: self.get_literal("number"),
      "organization": lambda: self.get_literal("organization"),
      "publisher": lambda: self.get_literal("publisher"),
      "school": lambda: self.get_literal("school"),
      "series": lambda: self.get_literal("series"),
      "title": lambda: self.get_literal("title"),
      "type": lambda: self.get_literal("type"),
      "url": lambda: self.get_literal("url"),
      "volume": lambda: self.get_literal("volume"),
      "author": lambda: self.get_authors('author'),
      "date": self.get_date,
      "editor": lambda: self.get_authors('editor'),
      # "journalabbrev": self.get_journalabbrev,
      "keywords": lambda: self.get_keyaff('keywords'),
      "pages": self.get_pages,   # From "PG"
        # "pubstate": lambda: self.get_literal("PST"),
      # For strings
      "definition": lambda: self.get_literal('definition')
    })

  def parse_fields(self, entry):
    """Parse an entry and return a dictionary with raw entries.
    Also sets the attribute LFields

    Parameters
    ----------
    entry : string


    Returns
    -------

    fields: dict
      Each element is a list with a cleaned-up value corresponding to the key in the original format.
    """

    def get_fields(strng):
      """Process a string from a bibtex entry and identify fields and values

      Parameters
      ----------
      strng : string

      strict : bool
        If True, only fields recognized by program bibtex are included  (Default value = False)

      Note
      ----

      Three possibilities are considered for format in values of fields:
        1. Surrounded by "{","}"
        2. Surrounded by '"'
        3. Not surrounded in the case of numbers or using strings


      Returns
      -------
      dict:
        Each element is a pair with key field and the corresponding value
      """
      ss = strng.strip()
      if not ss.endswith(','):  # Add the last commma if missing
        ss += ','

      ff = {}

      while True:
        name, sep, ss = ss.partition('=')
        name = name.strip().lower()  # This should be enough if there is no error in the entry
        if len(name.split()) > 1:   # Help recover from errors. name should be only one word anyway
          name = name.split()[-1]
        ss = ss.strip()
        if sep == '':
          break  # We reached the end of the entry

        if ss[0] == '{':    # The value is surrounded by '{}'
          s, e = helper.match_pair(ss)
          data = ss[s + 1:e - 1].strip()
        elif ss[0] == '"':  # The value is surrounded by '"'
          s = ss.find(r'"')
          e = ss.find(r'"', s + 1)
          data = ss[s + 1:e].strip()
        else:  # It should be a number or something involving a string
          e = ss.find(',')
          data = ss[0:e].strip()
          if not data.isdigit():  # Then should be some string
            dd = data.split('#')  # Test for joined strings
            if len(dd) > 1:
              for n in range(len(dd)):
                dd[n] = dd[n].strip()
                dd[n] = dd[n].replace('{', '"').replace('}', '"')
                if dd[n][0] != '"':
                  dd[n] = f'definitionofstring({dd[n]}) '
              data = '#'.join(dd)
            else:
              # JF: Change: replace standard abbreviations on import (mainly month abbreviations)
              data = helper.replace_abbrevs(f'definitionofstring({dd[0]}) ')
        s = ss[e].find(',')
        ss = ss[s + e + 1:]
        ff[name] = data
      return ff

    # We search work type
    itype = entry.find("{")
    fields = {"_type": entry[:itype].lower()}  # _type
    d = helper.match_pair(entry, start=itype - 1)  # Find start-end of entry
    if d:
      s = entry[d[0] + 1: d[1] - 1]

      if fields["_type"] == "string":
        name, defin = s.strip().split("=")
        defin = defin.strip().strip("{").strip("}")
        fields["_code"] = name.strip()
        fields["definition"] = defin.strip()
      else:
        ikey = s.find(",")  # Find the key and its position
        fields["_code"] = s[: ikey].strip()
        t = get_fields(s[ikey:])
        fields.update(t)

    # Clean-up and Convert each field value to unicode text
    for k in fields.keys():
      fields[k] = helper.repl_multiple_spaces(helper.bibtex_to_text(fields[k].strip()))
    self.LFields = fields
    return fields

  def get_keyaff(self, f):
    """Get the keywords

    Returns
    -------

    list: a list of keywords
    """
    k = self.get_literal(f)
    if k:
      return list(map(str.strip, k.split(",")))
    else:
      return []

  def get_keywords(self):
    """Get the keywords

    Returns
    -------

    list: a list of keywords
    """
    k = self.get_literal("keywords")
    if k:
      return list(map(str.strip, k.split(",")))
    else:
      return []

  def get_journalabbrev(self):
    """Get an abbreviation for the journal

    Returns
    -------

    str: The abbreviation
    """
    return helper.create_journal_abbrev(self.LFields)[1]

  def get_date(self):
    """Get the publication day

    Returns
    -------

    list:
      If there is a date, the date as a three-element list in format ['yyyy','mm','dd'].
        If any field is missing the corresponding element will be empty ''
      If there are none of  day,month,year it will return an empty list []
    """
    yy = mm = dd = ""

    y = self.get_literal('year')
    m = self.get_literal('month').lower()[:3]
    d = self.get_literal('day').lower()[:3]

    if not y and not m and not d:
      return []
      # return [yy, mm, dd]

    if y:
      yy = y.zfill(4)
    if m != "":
      if m.isdigit():
        mm = m.zfill(2)
      else:
        im = [x[0] for x in helper.strings_month].index(m)
        if im > 0:
          mm = f"{im + 1:02d}"
    if d != "":
      dd = d.zfill(2).strip()
    return [yy, mm, dd]

  def get_authors(self, who='author'):
    """Process a list of author in bibtex form and Returns a list of authors
    where each author is a list of the form:

                      [von, Last, First, Jr]

    Parameters
    ----------
    data : string
      Author list given in standard bibtex form


    Returns
    -------
    list:
      Each element is an author, described by a list
    """
    authors = self.get_literal(who)
    if authors:
      data = re.split(" and ", authors, flags=re.IGNORECASE)
      return list(map(helper.process_name, data))

  def get_pages(self):
    """Get the pages of the work

    Returns
    -------

  list: ['first page', 'last page', 'total pages'] each element type is
  string

    """
    fpage = lpage = tpage = ""
    pages = self.get_literal('pages')

    if pages:
      pp = re.split(r"\W+", pages)

      # First page
      fpage = pp[0]
      if fpage.isdigit():
        fp = int(fpage)

      # Last page if present
      if len(pp) > 1:
        lpage = pp[1]
        if lpage.isdigit():
          lp = int(lpage)

      # Add number of pages if possible
      try:
        tpage = str(lp - fp)
      except BaseException:
        tpage = ""

      return [fpage, lpage, tpage]


if __name__ == '__main__':

  # from pathlib import Path
  from io import StringIO

  bibinput = """
@string{nim = {Nuclear Instrument and Methods}}

@string{pr = {Physical Review}}

  @article{Kendric03JCP,
  author = {Brian K. Kendrick},
  journal = {J. Chem. Phys. A},
  volume = {119},
  number = {12},
  pages = {5805-5817},
  publisher = {AIP},
  year = {2003},
}

@article{Hansen03PRA,
  author = {J. P. Hansen and T. S{\\o}revik and L. B. Madsen},
  journal = pr # { A},
  volume = 68,
  pages = {031401(R)},
  year = 2003
}

@misc{etde_20082014,
title = {Uranium enrichment by laser: SILVA process}
author = {Rosengard, Ph Elias A.}
abstract = {About 15 years ago, the Atomic Energy Commissariat (AEC) has decided to develop uranium enrichment process SILVA based on selective photoionization of atomic vapor by laser. This decision has led to an important research programme, jointly carried out by the French AEC and the industrial operator Cogema. After having described the principle of this process and the various functions that production installation must fill up, we will also present in detail photons-atoms interaction aspects, and the interaction between laser and optics. (author)}
issue = {68}
place = {Syrian Arab Republic}
year = {2000}
month = {Aug},
}

@misc{etde_614515,
title = {The ``SILVA'' program; Le cycle du combustible evolution des techniques d`enrichissement isotopique }
author = {Jacob, M}
abstract = {The ``SILVA`` process (Uranium Atomic Vapor Laser Isotopic Separation) is an innovating system of enriched uranium (electronuclear reactors fuel) production. Its great interest comes from its selectivity. The aim is to divide by a factor of three the costs of production compared with those of the current plants. The stakes of this research program, its principle, the main research axis, the material means and the future prospects beyond 1997 are given as well as its technical advancement state. (O.M.).}
address = {France}
month = {Dec}
year = {1996}
}

@article{ Acuña2009288,
title = "Quantum-trajectory calculations of proton-hydrogen model collisions",
journal = "Nuclear Instruments and Methods in Physics Research Section B: Beam Interactions with Materials and Atoms",
volume = "267",
number = "2",
pages = "288 - 291",
year = "2009",
note = "Proceedings of the Fourth International Conference on Elementary Processes in Atomic Systems",
issn = "0168-583X",
doi = "DOI: 10.1016/j.nimb.2008.10.081",
url = "http://www.sciencedirect.com/science/article/B6TJN-4TVTJDX-2/2/dfacb206af8e281962f57bce4cadad6e",
author = "M. A. Acu{\\~n}a and J. Fiol",
keywords = "Quantum mechanics,Bohm, Quantum-trajectories, Stability matrix, Schrödinger equation, Inelastic collisions",
abstract = "
We investigate a proton-hydrogen model collision using a method based on the de Broglie-Bohm formulation of quantum dynamics. By studying the quantum-trajectories of the particles we obtain approximate ionization and capture cross sections that are in good agreement with the exact values. In particular, the implementation of this high-order approximation method allows us to solve each trajectory independently. The method has a relatively low computational cost and can be straightforwardly parallelized for many bodies systems."
}

"""

  # fname = Path("ejemplos/ejemplo1.nbib")
  fname = StringIO(bibinput)
  B = BibtexParser()
  bib = B.parse_file(fname)
  for v in bib.values():
    print(v)
