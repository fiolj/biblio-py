#!/usr/bin/env python
import re
# from . import helper
from yapbib import helper


"""
Set of routines to parse bibliographic data and return each entry as a dictionary
It is mainly intended as a helper file to the Class BibItem (see bibitem.py)
but can be used as a standalone script

USAGE:


"""


class BiblioParser(object):
  """Bibliography simple parser

  """

  def __init__(self):
    super(BiblioParser, self).__init__()
    # Mapping to convert to internal format
    self.field_translator = {k: self.Null for k in helper.field_description}
    self.LFields = {k: [] for k in helper.field_description}
    # Regular expression for parsing of the entry. Replace in child class
    self.reg_field = re.compile("[\\W+]$", re.MULTILINE | re.UNICODE)
    self.reg_begin = re.compile("@", re.UNICODE)

  def Null(self):
    return None

  def get_literal(self, Field):
    """Get from the list the value for literal fields

    Parameters
    ----------
    Field : Field string

    Returns
    -------

    string: matching value
    """
    d = self.LFields.get(Field, [""])
    # print(d)
    # return "".join(d)
    if len(d) == 1:
      return d[0]
    else:
      return d

  def get_firstliteral(self, Fields):
    """Get the first field from a list of Fields

    Parameters
    ----------
    Fields : Fields List

    Returns
    -------

    string: the first matching value
    """

    for f in Fields:
      d = self.get_literal(f)
      if d:
        return d

  def separate_entries(self, page):
    """Reads a string and identifies bibliographic entries

    Parameters
    ----------
    entry : string

    Returns
    -------

    item: list
       list of strings with the text of each entry
    """

    page = helper.handle_math(page, 1)
    t = self.reg_begin.split(page)[1:]
    return [u + v for u, v in zip(t[::2], t[1::2])]

  def parse_fields(self, entry):
    """Parse an entry and return a dictionary with raw entries.
    Also sets the attribute LFields

    Parameters
    ----------
    entry : string


    Returns
    -------

    fields: dict
      Each element is a list with a cleaned-up value corresponding to the key in the original format.
    """

    t = self.reg_field.split(entry)[1:]
    fields = {}
    for j, k in enumerate(t[::2]):
      k = k[:4].strip()
      v = " ".join(t[2 * j + 1].split())
      if k in fields:
        fields[k] += [v]
      else:
        fields[k] = [v]
    self.LFields = fields
    return fields

  def parse_entry(self, entry):
    """Reads an entry and returns a dictionary with the parsed data

    Parameters
    ----------
    entry : string

    Returns
    -------

    item: dict
      Each element is the value of the corresponding field
    """

    self.parse_fields(entry)
    # for k in self.field_translator:
    #   if self.field_translator[k]():
    #     print(f"{k}: {self.field_translator[k]()}")
    return {k:
            self.field_translator[k]() for k in self.field_translator if self.field_translator[k]()
            }

  def parse_file(self, fname):
    """Reads a file and returns a dictionary with all entries

    Parameters
    ----------
    fname : string or file handler

    Returns
    -------

    item: dict
      Each element is the value of the corresponding field
    """
    fi = helper.openfile(fname)
    page = fi.read()
    helper.closefile(fi)
    return self.parse_data(page)

  def parse_data(self, page):
    """Reads a file and returns a dictionary with all entries

    Parameters
    ----------
    fname : string or file handler

    Returns
    -------

    item: dict
      Each element is the value of the corresponding field
    """
    b = {}
    entries = self.separate_entries(page)
    for e in entries:
      d = self.parse_entry(e)

      if helper.verify_entry(d):
        b[d["_code"]] = d

    return b
